/*
 *    Copyright 2016, Arthur Teplitzki, 2013, Edmodo, Inc.
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.theartofdev.edmodo.cropper;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * @Author Zhao Baiyi
 *
 * 此类为一个工具类
 *
 * 提供了查询windows，也就是屏幕宽和高的方法
 */

final class CropWindowHandler {

    //记录一个上下文环境
    private Context mContext;

    //构造器
    public CropWindowHandler(Context context){
        mContext = context;
    }

    //返回屏幕宽度
    public int getWindowWidth(){
        //获取手机屏幕大小
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(mContext).get();
        DisplayAttributes displayAttributes = display.getAttributes();

        return displayAttributes.width;
    }

    //返回屏幕高度
    public int getWindowHeight(){
        //获取手机屏幕大小
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(mContext).get();
        DisplayAttributes displayAttributes = display.getAttributes();

        return displayAttributes.height;
    }
}
