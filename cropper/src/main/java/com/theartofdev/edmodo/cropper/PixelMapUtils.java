/*
 *    Copyright 2016, Arthur Teplitzki, 2013, Edmodo, Inc.
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.theartofdev.edmodo.cropper;

import ohos.app.Context;
import ohos.global.resource.*;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;

import java.io.IOException;
import java.util.Optional;

/**
 * @Author Zhao BaiYi
 *
 * 这是一个位图的工具类，用来存放一张位图，并且可以计算并返回位图的宽高，位图所在的绝对位置等等
 */

final class PixelMapUtils {

    //位图
    private PixelMap mPixelMap;

    //上边距
    private int top;

    //屏幕工具类
    private CropWindowHandler mCropWindowHandler;

    //图片的id
    private int mId;

    //构造器，用来初始化位图工具类
    public PixelMapUtils(Context context , PixelMap pixelMap , int top , int id){
        //位图
        mPixelMap = pixelMap;

        //上边距
        this.top = top;

        //屏幕工具类
        mCropWindowHandler = new CropWindowHandler(context);

        //图片id
        mId = id;
    }

    //返回屏幕宽度 因为图片缩放到屏幕的宽度
    public int getPixelMapWidth(){
        return mCropWindowHandler.getWindowWidth();
    }

    //返回图像真实宽度
    public int getRealPixelMapWidth(){
        return mPixelMap.getImageInfo().size.width;
    }

    //返回屏幕宽度 因为图片缩放到屏幕的宽度
    public int getPixelMapHeight(){
        return mCropWindowHandler.getWindowWidth();
    }

    //返回图像真实高度
    public int getRealPixelMapHeight(){
        return mPixelMap.getImageInfo().size.height;
    }

    //根据id获取图片的pixelmap
    public static Optional<PixelMap> getOriginalPixelMap(Context context, int id){
        String path = getPathById(context, id);
        if (path == null || path.isEmpty()) {
            return Optional.empty();
        }

        RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        options.formatHint = "image/jpg";
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        decodingOptions.editable = true;
        try {
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions));


        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    //根据id获取相对路径
    private static String getPathById(Context context, int id){
        String path = "";
        if (context == null){
            return path;
        }

        ResourceManager manager = context.getResourceManager();
        if (manager == null){
            return path;
        }

        try {
            path = manager.getMediaPath(id);

        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    //获取翻转后的pixelmap
    public PixelMap getFlipPixelMap(){
        //定义一个数组用来取得像素值
        int[] pixels = new int[getPixelMapWidth() * getPixelMapHeight()];

        //定义矩形来设定取得的位置
        Rect rect1 = new Rect(0 , 0 , getPixelMapWidth() , getPixelMapHeight()/2);
        mPixelMap.readPixels(pixels , 0 , getPixelMapWidth() , rect1);

        //把像素转换成二维数组
        int[][] pixels2 = new int[getPixelMapHeight()/2][getPixelMapWidth()];
        int row = 0;
        for(int i = 0 ; i < (getPixelMapHeight()/2 * getPixelMapWidth()) ; i++){
            if((i % getPixelMapWidth() == 0) && (i != 0)){
                row++;
            }
            pixels2[row][i - row * getPixelMapWidth()] = pixels[i];

        }

        //把二维数组转换成一维
        int[] pixelsResult = new int[getPixelMapWidth() * getPixelMapHeight()];
        row = 0;
        for(int i = 0 ; i < (getPixelMapHeight()/2 * getPixelMapWidth()) ; i++){
            if((i % getPixelMapWidth() == 0) && (i != 0)){
                row++;
            }
            pixelsResult[i] = pixels2[row][i - row * getPixelMapWidth()];
        }

        Rect rect2 = new Rect(0 , getPixelMapHeight()/2 , getPixelMapWidth() , getPixelMapHeight()/2);
        mPixelMap.writePixels(pixelsResult , 0 ,getPixelMapWidth() , rect2);

        return mPixelMap;
    }

    //返回图片当前的left位置（相对于整个屏幕）
    public int getPositionLeft(){
        return mCropWindowHandler.getWindowWidth()/2 - getPixelMapWidth()/2;
    }

    //返回图片当前的top位置（相对于整个屏幕）
    public int getPositionTop(){
        return top;
    }

    //返回图片当前的Right位置（相对于整个屏幕）
    public int getPositionRight(){
        return mCropWindowHandler.getWindowWidth()/2 + getPixelMapWidth()/2;
    }

    //返回图片当前的top位置（相对于整个屏幕）
    public int getPositionBottom(){
        return top + getPixelMapHeight();
    }

    //返回图片Id
    public int getmId(){
        return mId;
    }
}
