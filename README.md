# ImageCropper_ohos

#### 项目介绍

- 项目名称：图片裁剪项目
- 所属系列：鸿蒙的第三方组件适配移植
- 功能：提供一个AbilitySlice界面，通过代码跳转。此界面包含了被裁减的图片以及一个裁剪框，此界面支持对一个正方形的图片进行裁剪，支持对正方形图片的旋转、水平翻转、垂直翻转等功能，支持返回到用户输入的包名加类名的Ability，支持返回一个裁剪后的PixelMap或是一个包含裁剪后图片信息的Component
- 项目移植状态：完成
- 调用差异：无
- 开发版本：sdk5，DevEco Studio2.1 beta3
- 项目作者和维护人：赵柏屹
- 邮箱：isrc_hm@iscas.ac.cn
- 社区文章地址：https://harmonyos.51cto.com/posts/3254
- 原项目Doc地址：https://github.com/ArthurHub/Android-Image-Cropper

#### 项目介绍

- 编程语言：Java 

#### 安装教程

1. 下载裁剪jar包cropper.jar。
2. 启动 DevEco Studio，将下载的jar包，导入工程目录“entry->libs”下。

<img src="https://images.gitee.com/uploads/images/2021/0127/192920_62d36568_8611846.png" alt="输入图片说明" style="zoom:60%;" />

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
4. 在导入的jar包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

<img src="https://images.gitee.com/uploads/images/2021/0127/193928_c70abe72_8611846.png" alt="输入图片说明" style="zoom:60%;" />

<img src="https://images.gitee.com/uploads/images/2021/0127/193948_90b60358_8611846.png" alt="输入图片说明" style="zoom:60%;" />


如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，

并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1. 通过如下代码跳转到组件提供的AbilitySlice——CropImageAbility

   ```java
   CropImage.activity()
                   .setContext(this)
                   .setSource(ResourceTable.Media_cat_square)
                   .setBundleName("com.huawei.mytestproject")
                   .setAbilityName("com.huawei.mytestproject.MainAbility")
                   .setRequset_code(1234)
                   .start(super.getAbility(),this);
   ```

   注：

   activity() 方法为初始化类。

   setContext() 方法需要传入一个上下文环境。

   setResource() 方法需要传入期待被裁减图片的id （目前只能使用在ResourceTable中注册过的图片，并且必须为正方形图片，图片位置如下所示）。

   <img src="https://images.gitee.com/uploads/images/2021/0127/194037_66e86871_8611846.png" alt="输入图片说明" style="zoom:60%;" />

   <img src="https://images.gitee.com/uploads/images/2021/0127/194108_b0d85943_8611846.png" alt="输入图片说明" style="zoom:60%;" />

   setBundleName() 与setAbilityName() 方法即为跳转到裁剪页面以后，想跳转回来的界面的包名以及类名 。如在MainAbilitySlice中写了这段代码并执行，就会跳转到组件自带的AbilitySlice，组件自带的界面也有一些按钮实现跳转回来的方法，所以需要设置想要跳转回来的Ability的名字。

   setRequest_code() 方法则为请求参数，如例子填写即可。

   start() 方法需要传入ability和context。
   
2. 在组件提供的界面中进行裁剪图片翻转图片等操作

3. 组件界面会根据setBundleName() 与setAbilityName()传入的包名和类名进行跳转。

   跳转回主页面时intent中默认塞入了两个参数cropFlag，cropStatus。cropFlag为布尔型变量，表示是否裁剪成功。cropStatus为int型变量，表示裁剪状态。0为默认值，表示还未进行页面跳转，1表示取消裁剪，2表示裁剪成功，intent中有数据。可以参考如下示例进行使用，或者自定义其他功能实现。

   ```java
   if(intent.getIntParam("cropStatus",0) == 0){
       text.setText("欢迎使用");
   }else if(intent.getIntParam("cropStatus",0) == 1){
       text.setText("剪裁取消");
   }else if(intent.getIntParam("cropStatus",0) == 2){
       text.setText("剪裁成功");
   }
   ```
   
4. 获得裁剪结果

   裁剪后组件提供了两个方法。

   方法一：

   CropImage.handleImage(int result_code ,Component image)

   参数 int result_code（可以从intent中获得,如下所示）

   ```java
   int result_code = result.getIntParam("result_code" , 0);
   ```

   参数Component image 需要传入一个空的新new的Component。

   此方法会对component进行处理，塞入裁剪后的图片，以及图片旋转等等。

   方法二：

   ```java
   PixelMap croppedPixelMap = CropImage.getCroppedPixelMap();
   ```

   此方法可以返回裁剪后的PixelMap，用户可以根据需要自行进行处理。
   
5. MainAbilitySlice的具体使用可以参考如下代码：

   ```java
   public class MainAbilitySlice extends AbilitySlice {
   
       //定义一个图片
       Component image;
       //定义一个文本
       Text text;
   
       @Override
       public void onStart(Intent intent) {
   
           //重写onstart方法并加载布局文件
           super.onStart(intent);
           super.setUIContent(ResourceTable.Layout_MainAbility_layout);
   
           //获取图片对象对应的component
           image = findComponentById(ResourceTable.Id_result_image);
   
           /*
            * 如果接收的cropFlag为true
            * 处理剪裁后的图片
            * 否则跳过
            */
           if(intent.getBooleanParam("cropFlag",false)){
               handleCrop(intent);
           }
   
           /* 自定义--获取文本对象对应的component
            * 根据intent里面的cropStatus来显示不同的文本
            * 0表示未接收到数据
            * 1表示剪裁取消
            * 2表示剪裁成功 有数据
            */
           text = (Text) findComponentById(ResourceTable.Id_text);
           if(intent.getIntParam("cropStatus",0) == 0){
               text.setText("欢迎使用");
           }else if(intent.getIntParam("cropStatus",0) == 1){
               text.setText("剪裁取消");
           }else if(intent.getIntParam("cropStatus",0) == 2){
               text.setText("剪裁成功");
           }
   
           //获取button对象对应的component
           Button button = (Button) findComponentById(ResourceTable.Id_button);
           // 设置button的属性及背景
           ShapeElement background = new ShapeElement();
           background.setRgbColor(new RgbColor(0, 125, 255));
           background.setCornerRadius(25);
           button.setBackground(background);
           if (button != null) {
               // 绑定点击事件
               button.setClickedListener(new Component.ClickedListener() {
                   public void onClick(Component v) {
                       begincrop();
                   }
               });
           }
       }
   
       public void begincrop(){
           CropImage.activity()
                   .setContext(this)
                   .setSource(ResourceTable.Media_cat_square)
                   .setBundleName("com.huawei.mytestproject")
                   .setAbilityName("com.huawei.mytestproject.MainAbility")
                   .setRequset_code(1234)
                   .start(super.getAbility(),this);
       }
   
       //处理剪裁结果
       private void handleCrop(Intent result) {
           int resultImg = result.getIntParam("resultImg",0);
           int result_code = result.getIntParam("result_code" , 0);
           if(resultImg != 0){
               CropImage.handleImage(result_code , image);
               //PixelMap croppedPixelMap = CropImage.getCroppedPixelMap();
           }
       }
   
       @Override
       public void onActive() {
           super.onActive();
       }
   
       @Override
       public void onForeground(Intent intent) {
           super.onForeground(intent);
       }
   }
   ```
#### 版本迭代

- v0.2.0-alpha
修复了由于module config文件问题导致的项目不可用

- v0.1.0-alpha


#### 版权和许可信息
- ImageCropper_ohos经过[Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0)授权许可。